/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra.request;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Stack;

public class GetRequest extends Request {
    private final HashMap<String, String> getRequestData;

    public GetRequest(String subResource, HashMap<String, String> getRequestData) {
        super(subResource);
        this.getRequestData = getRequestData;
    }

    public HashMap<String, String> getGetRequestData() {
        return getRequestData;
    }

    public String getEncodedGetData() {
        Stack<String> pairs = new Stack<>();
        for (String key :
                getRequestData.keySet()) {
            try {
                pairs.push(URLEncoder.encode(key, "UTF-8") + "=" + URLEncoder.encode(getRequestData.get(key).replaceAll("[\\t\\n\\r]+", "").trim(), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return "?" + String.join(",", pairs);
    }
}
