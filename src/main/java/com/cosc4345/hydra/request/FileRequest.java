/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra.request;

import java.util.HashMap;

public class FileRequest extends GetRequest {
    private final String filename;

    public FileRequest(String subResource, HashMap<String, String> getData, String filename) {
        super(subResource, getData);
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }
}
