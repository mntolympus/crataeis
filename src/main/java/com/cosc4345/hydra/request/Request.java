/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra.request;

import com.cosc4345.hydra.Main;

//TODO: Add RequestType checking
public abstract class Request {
    private final String subResource;
    private volatile String output;
    private volatile boolean isCompleted;

    Request(String subResource) {
        this.subResource = subResource;
        isCompleted = false;
    }

    private boolean isCompleted() {
        return isCompleted;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
        isCompleted = true;
    }

    @Override
    public String toString() {
        return "Request{" +
                "subResource='" + subResource + '\'' +
                ", output='" + output + '\'' +
                ", isCompleted=" + isCompleted +
                '}';
    }

    public String getSubResource() {
        return subResource;
    }

    public void waitForOutput() {
        while (!this.isCompleted() && Main.systemIsRunning) {
            try {
                Thread.sleep(Main.waitPollRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
