package com.cosc4345.hydra;

import com.cosc4345.hydra.request.FileRequest;
import com.cosc4345.hydra.request.GetRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by TJ Challstrom on 26-Nov-17 at 03:37 PM.
 * Dispenser goes here!
 */
public class Main {
    public static volatile boolean systemIsRunning = true;
    public static final int waitPollRate = 1000;

    public static void main(String[] args) {
        ExecutorService backgroundExecutor = Executors.newCachedThreadPool();
        backgroundExecutor.submit(Web.getInstance());
        File jarDir = FileHelper.getJarDir();
        if (jarDir != null) {
            List<File> currentFiles = Arrays.asList(jarDir.listFiles());
            Pattern scPattern = Pattern.compile("scylla-controller-(\\d\\.\\d\\.\\d)-*\\w*-FULL.jar");
            Map<String, String> versions = new HashMap<>();
            for (File file :
                    currentFiles) {
                String filename = file.getName();
                Matcher matcher = scPattern.matcher(filename);
                if (matcher.matches()) {
                    versions.put(matcher.group(1), filename);
                }
            }
            System.out.println("-----Versions installed-----");
            for (String version :
                    versions.keySet()) {
                System.out.println(version);
            }
            System.out.println("---------------------------");
            String latestFile = getLatestVersion();
            System.out.println("Latest File: " +latestFile);
            Matcher reqMatcher = scPattern.matcher(latestFile.trim());
            if (reqMatcher.matches()) {
                String latestVersion = reqMatcher.group(1);
                if (versions.size() > 0) {
                    String localLatest = Collections.max(versions.keySet());
                    if (Objects.equals(latestVersion, localLatest)) {
                        System.out.println("Latest version recognized, attempting verification.");
                        String remoteHash = getVersionHash(latestVersion);
                        File localFile = new File(versions.get(localLatest));
                        String localHash = FileHelper.getOtherMD5(localFile);
                        if (Objects.equals(remoteHash, localHash)) {
                            System.out.println("Latest version verified. Spawning Controller.");
                            startController(localFile, args);
                        } else {
                            System.out.println("Latest version verification FAILED!");
                            System.out.println("Removing and replacing local version!");
                            localFile.delete();
                            requestLatestFile(versions.get(latestVersion));
                            startController(new File(latestFile), args);
                        }
                    } else {
                        System.out.println("Latest local version is behind latest remote version! Updating...");
                        requestLatestFile(latestFile);
                        startController(new File(latestFile), args);
                    }
                } else {
                    System.out.println("No local versions detected! Downloading latest...");
                    String latest = getLatestVersion();
                    requestLatestFile(latest);
                    startController(new File(latestFile), args);
                }
            }
        }
        systemIsRunning = false;
        backgroundExecutor.shutdown();
    }
    private static void startController(File localFile, String[] args) {
        System.out.println("java -jar " + localFile.getAbsolutePath() + " " + String.join(" ", args));
        ProcessBuilder builder = new ProcessBuilder("java", "-jar", localFile.getAbsolutePath(), String.join(" ", args));
        builder.inheritIO();
        try {
            builder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void requestLatestFile(String filename) {
        HashMap<String, String> localLatestRequestData = new HashMap<>();
        try {
            URL website = new URL("http://cosc4345-team4.com/dl/dl.php?jarFile="+filename);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            FileOutputStream fos = new FileOutputStream(filename);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private static String getLatestVersion() {
        HashMap<String, String> latestGetRequestData = new HashMap<>();
        latestGetRequestData.put("version", "latest");
        GetRequest latestRequest = new GetRequest("dl/version.php", latestGetRequestData);
        Web.getInstance().queueRequest(latestRequest);
        latestRequest.waitForOutput();
        return latestRequest.getOutput();
    }

    private static String getVersionHash(String version) {
        HashMap<String, String> latestGetRequestData = new HashMap<>();
        latestGetRequestData.put("version", version);
        GetRequest latestRequest = new GetRequest("dl/version.php", latestGetRequestData);
        Web.getInstance().queueRequest(latestRequest);
        latestRequest.waitForOutput();
        return latestRequest.getOutput();
    }
}
